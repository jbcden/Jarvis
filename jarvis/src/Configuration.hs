module Configuration
( getConfigs
, Config (..)
) where

import GHC.Generics
import Data.Yaml as Y
import Data.Aeson
import System.Directory
import Control.Exception

data Config = Config { username :: String
                     , password :: String
                     } deriving (Show, Generic)

instance FromJSON Config
instance ToJSON Config

getConfigs :: IO Config
getConfigs = do
  configFile <- loadConfigFile
  case configFile of
    Just config -> return config
    Nothing -> promptConfig

loadConfigFile :: IO (Maybe Config)
loadConfigFile = do
  configPath <- configFilePath
  configExists <- doesFileExist configPath
  if configExists
     then Y.decodeFile configPath
     else return Nothing


promptConfig :: IO Config
promptConfig = do
  username <- promptUsername
  pass <- promptPassword
  let config =  Config { username = username, password = pass }
  writeConfigFile config
  return config

promptUsername :: IO String
promptUsername = do
  putStrLn "What is your YouTrack username"
  name <- getLine
  return name

promptPassword :: IO String
promptPassword = do
  putStrLn "What is your YouTrack password"
  pass <- getLine
  return pass

writeConfigFile :: Config -> IO ()
writeConfigFile config = do
  configPath <- configFilePath
  Y.encodeFile configPath config

configFilePath :: IO String
configFilePath = do
  home <- getHomeDirectory
  return $ home ++ "/.jarvis"
