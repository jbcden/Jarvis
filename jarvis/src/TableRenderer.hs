module TableRenderer
(printTable
) where

import Data.List (intersperse, intercalate)

printTable :: [(String, a -> String)] -> [a] -> IO ()
printTable columns [] = do
  putStrLn "nothing"
  return ()
printTable columns originalSource = do
  putStrLn $ unlines
           $ (renderHeaders columns originalSource)
           : (renderAlignments columns originalSource)
           : (renderRows columns originalSource)
  return ()
  where renderHeaders columns source = concat $ intersperse "| " $ map (padHeader source) columns
        padHeader source (header, f) = pad  header (columnWidth f originalSource) ' '
        renderAlignments columns source = concat $ intersperse "| " $ map (padAlignment source) columns
        padAlignment source (_, f) = pad ":"  (columnWidth f originalSource) '-'
        renderRows _ [] = []
        renderRows columns source = renderRow columns source : (renderRows columns $ tail source)
        renderRow columns source = concat $ intersperse "| " $ map (padRow source) columns
        padRow source (_, f) = pad (f $ head source) (columnWidth f originalSource) ' '

width :: [String] -> Int
width = foldl (max) 0 . map (length . show)

pad :: [a] -> Int -> a -> [a]
pad content columnWidth padChar = content ++ replicate (columnWidth - (length content)) padChar

columnWidth :: (a -> String) -> [a] -> Int
columnWidth f source = (width $ map f source)
