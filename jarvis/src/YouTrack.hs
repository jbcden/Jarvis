module YouTrack
( withYouTrackSession
, issuesAssignedToMe
, issuesReviewedByMe
, issuesPairingWithMe
) where

import Control.Arrow
import Control.Lens
import Control.Monad
import Data.Aeson
import Data.Maybe
import Data.Tree.NTree.TypeDefs
import Network.HTTP.Base
import Network.Wreq
import Text.XML.HXT.Core

import qualified Control.Arrow.ArrowTree as AT
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString.Lazy.Char8 as Char8
import qualified Network.Wreq.Session as S

import Configuration
import Issue
import TableRenderer

baseUrl = "https://youtrack.mobiledefense.com"
authPath = "/rest/user/login?login="
issuesPath = "/rest/issue?filter="

withYouTrackSession :: Config -> (S.Session -> IO a) -> IO a
withYouTrackSession config requests = S.withSession $ \sess -> do
  let name = urlEncode $ username config
  let pass = urlEncode $ Configuration.password config

  S.post sess (baseUrl ++ authPath ++ name ++ "&password=" ++ pass) (toJSON ("" :: String))
  requests sess

queryYoutrackWithFilter :: S.Session -> String -> IO [Issue]
queryYoutrackWithFilter sess filter = do
  r <- S.get sess (baseUrl ++ issuesPath ++ filter)
  let body =  r ^. responseBody
  let xml = Char8.unpack body
  parseIssues xml

issuesAssignedToMe :: S.Session -> IO [Issue]
issuesAssignedToMe sess = do
  queryYoutrackWithFilter sess "%23%7BAssigned%20to%20me%7D%20%23Unresolved&max=500"

issuesReviewedByMe :: S.Session -> IO [Issue]
issuesReviewedByMe sess = do
  queryYoutrackWithFilter sess "Reviewed+by%3A+me+%23Unresolved+&max=500"

issuesPairingWithMe :: S.Session -> IO [Issue]
issuesPairingWithMe sess = do
  queryYoutrackWithFilter sess "Pairing+with%3A+me+%23Unresolved+&max=500"

parseIssues :: String -> IO [Issue]
parseIssues xml =
  runX $ readString [] xml
    >>>
      AT.deep (isElem >>> hasName "issue")
    >>>
  proc issues -> do
  issueId <- (getAttrValue "id") -< issues
  summary <- (parseIssueField "summary" $ getChildren) -< issues
  description <- (parseIssueField "description" $ getChildren) -< issues
  priority <- (parseIssueField "Priority" $ getChildren) -< issues
  state <- (parseIssueField "State" $ getChildren) -< issues
  subsystem <- (parseIssueField "Subsystem" $ getChildren) -< issues
  assignee <- (parseIssueField "Assignee" $ getChildren) -< issues
  reviewer <- ((parseIssueField "Reviewed by" $ getChildren) >>> arr Just) `orElse` (constA Nothing) -< issues
  pairing <- ((parseIssueField "Pairing with" $ getChildren) >>> arr Just) `orElse` (constA Nothing) -< issues
  returnA -< Issue { issueId = issueId
                   , summary = summary
                   , description = description
                   , priority = priority
                   , state = state
                   , subsystem = subsystem
                   , assignee = assignee
                   , reviewer = reviewer
                   , pairing = pairing
                   }

parseIssueField :: ArrowXml cat => String -> cat a XmlTree -> cat a String
parseIssueField field issues = issues
  >>>
  (isElem >>> hasName "field")
  >>>
  (hasAttrValue "name" (== field))
  >>>
  (getChildren >>> isElem >>> hasName "value")
  >>>
  getChildren
  >>>
  getText
