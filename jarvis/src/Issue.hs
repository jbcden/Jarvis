module Issue
( Issue (..)
) where

data Issue = Issue { issueId :: String
                   , summary :: String
                   , description :: String
                   , priority :: String
                   , assignee :: String
                   , reviewer :: Maybe String
                   , pairing :: Maybe String
                   , state :: String
                   , subsystem :: String
                   } deriving (Show)
