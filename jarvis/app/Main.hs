module Main where

import Configuration
import YouTrack
import TableRenderer(printTable)
import Issue
import System.Environment (getArgs)

import Data.Maybe

main :: IO ()
main = do
  args <- getArgs
  dispatch args
  return ()

dispatch :: [String] -> IO ()
dispatch args = case args of
  "youtrack":args -> case args of
    "list":args -> listIssues
    args -> putStrLn $ "Youtrack does not support " ++ concat args
  args -> putStrLn $ "jarvis does not support " ++ concat args
  

listIssues = do
  config <- getConfigs

  let header = [ ("Name", issueId)
               , ("Summary", summary)
               , ("State", state)
               , ("Reviewer", fromMaybe "Unassigned" . reviewer)
               ]

  withYouTrackSession config $ \session -> do
    putStrLn "Assigned to you:"
    myIssues <- issuesAssignedToMe session
    printTable header myIssues

    putStrLn "Reviewed by me:"
    myReviews <- issuesReviewedByMe session
    printTable header myReviews

    putStrLn "Paired with me:"
    myPairs <- issuesPairingWithMe session
    printTable header myPairs

    return ()
  
